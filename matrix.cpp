#include "matrix.h"
#include <iostream>
using namespace std;

template<>
aghMatrix<char*> aghMatrix<char*>::operator+ (const aghMatrix &to_add) const
{
    if ((rows != to_add.rows) || (columns != to_add.columns))
    {
        throw aghException(0, "inadequate matrices size", "matrix.cpp", 10);
    }

    else
    {
        aghMatrix<char*> wynik(rows, columns);
        char *tmp1, *tmp2;

        for (int i = 0; i < rows; i++)
            for (int j = 0; j < columns; j++)
            {
                tmp1 = matrix[i][j];
                tmp2 = to_add.matrix[i][j];
                wynik.setItem(i, j, add(tmp1, tmp2));
            }

        return wynik;
    }
}

template<>
aghMatrix<char*> aghMatrix<char*>::operator* (const aghMatrix &to_mul) const
{
    if (columns != to_mul.rows)
    {
        throw aghException(0, "inadequate matrices size", "matrix.cpp", 31);
    }

    else
    {
        aghMatrix<char*> wynik(rows, to_mul.columns);
        char **tmp_wynik = new char*[columns];

        for (int i = 0; i < rows; i++)
            for (int j = 0; j < to_mul.columns; j++)
            {
                for (int k = 0; k < columns; k++)
                    tmp_wynik[k] = mul(matrix[i][k], to_mul.matrix[k][j]);

                for (int k = 0; k < columns; k++)
                    tmp_wynik[0] = add(tmp_wynik[0], tmp_wynik[k]);

                wynik.setItem(i, j, tmp_wynik[0]);
            }

        delete[] tmp_wynik;

        return wynik;
    }
}

template <>
bool aghMatrix<char*>::operator== (const aghMatrix &wzor) const
{
    if (this == &wzor)
        return true;

    if ((this->rows != wzor.rows) || (this->columns != wzor.columns))
        return false;

    if ((matrix == NULL) || (wzor.matrix == NULL))
        return false;

    for (int i = 0; i<rows; i++)
        for (int j = 0; j < columns; j++)
        {

            int length1 = 0, length2 = 0;
            for (int k = 0; matrix[i][j][k] != '\0'; k++)
                length1++;
            for (int k = 0; wzor.matrix[i][j][k] != '\0'; k++)
                length2++;
            if (length1 != length2)
                return false;

            for (int k = 0; k < length1; k++)
                if (matrix[i][j][k] != wzor.matrix[i][j][k])
                    return false;
        }

    return true;
}

template <>
char* aghMatrix<char*>::operator() (int r, int c)
{
    if (r<0 || r>rows || c<0 || c>columns)
        throw aghException(0, "inadequate matrices size", "matrix.h", 261);
    else
    {
        char *tmp = matrix[r][c];
        int length = 0;

        for (int i = 0; tmp[i] != '\0'; i++)
            length++;

        char *wynik = new char[length];
        for (int i = 0; i < length; i++)
            wynik[i] = tmp[i];
        wynik[length] = '\0';

        return wynik;

    }
}

template<>
aghMatrix<char> aghMatrix<char>::operator+ (const aghMatrix &to_add) const
{
    if (rows != to_add.rows || columns != to_add.columns)
    {
        throw aghException(0, "inadequate matrices size", "matrix.cpp", 116);
    }
    else
    {
        aghMatrix<char> wynik(rows, columns);
        for (int i = 0; i<rows; i++)
            for (int j = 0; j<columns; j++)
                wynik.matrix[i][j] = char( ( ( int(matrix[i][j]) + int(to_add.matrix[i][j]) - 194) % 26) + 97 );
        return wynik;
    }
}

template<>
aghMatrix<char> aghMatrix<char>::operator* (const aghMatrix &mult) const
{
    if (columns != mult.rows)
    {
        throw aghException(0, "inadequate matrices size", "matrix.cpp", 136);
    }
    else
    {
        aghMatrix<char> wynik(rows, mult.columns);
        for (int i = 0; i<rows; i++)
            for (int j = 0; j<mult.columns; j++)
            {
                wynik.matrix[i][j] = 'a';           //inicjalizuje 'a', ktore symbolizuja zera (bo ponizej odejmujemy 97)
                for (int k = 0; k<columns; k++)
                    wynik.matrix[i][j] = char( ((int(wynik.matrix[i][j])-97 + ((int(matrix[i][k])-97) * (int(mult.matrix[k][j])-97))) % 26) + 97);
            }
        return wynik;
    }
}

template <>
void aghMatrix<char>::setItems(int r, int c, ...)
{
    resize(r, c);
    int counter = r*c;
    char value;
    int _rows = 0, _columns = 0;

    va_list values;
    va_start(values, c);
    for (int i = 0; i < counter; i++)
    {
        value = va_arg(values, int);
        if ((_rows < rows) && (_columns < columns))
        {
            matrix[_rows][_columns] = value;
            _columns++;
        }
        else
        {
            _rows++;
            _columns = 0;
            matrix[_rows][_columns] = value;
            _columns++;
        }
    }
    va_end(values);
}
