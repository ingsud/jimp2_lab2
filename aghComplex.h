#ifndef aghComplex_H
#define aghComplex_H

class aghComplex{
private:
    double re, im;
public:
    aghComplex(double re = 0, double im = 0);
    void print() const;
    aghComplex operator+ (const aghComplex &y) const;
    aghComplex operator* (const aghComplex &y) const;
    aghComplex& operator= (const aghComplex &y);
    aghComplex& operator= (const int &value);
    bool operator== (const aghComplex &y) const;
    bool operator!= (const aghComplex &y) const;
};

#endif // aghComplex_H
