#include "aghComplex.h"
#include<iostream>
#include<cmath>
using namespace std;

aghComplex::aghComplex(double x, double y) : re(x), im(y){}

void aghComplex::print() const{
    cout.width(3);
    if (im != 0) cout << re << "i" << im;
    else cout << re;
}

aghComplex aghComplex::operator+ (const aghComplex &y) const{
    aghComplex suma;
    suma.re = re + y.re;
    suma.im = im + y.im;
    return suma;
}

aghComplex aghComplex::operator* (const aghComplex &y) const{
    aghComplex iloczyn;
    iloczyn.re = re*y.re - im*y.im;
    iloczyn.im = re*y.im + im*y.re;
    return iloczyn;
}

bool aghComplex::operator== (const aghComplex &y) const{
    if (re == y.re && im == y.im) return true;
    return false;
}

aghComplex& aghComplex::operator= (const aghComplex &y){
    if (this == &y) return *this;
    else
    {
        re = y.re;
        im = y.im;
        return *this;
    }
}

aghComplex& aghComplex::operator= (const int &value)
{
    re = 0;
    im = 0;

    return *this;
}

bool aghComplex::operator!= (const aghComplex &y) const{
    return !(*this == y);
}
